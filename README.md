# README #

This repository contains blender version, with modified version of bullet used for underwater simulation.

Compiled version is in the file `blender.tar.gz`

The compiled version require boost libraries. Theese can be downloaded using `sudo apt-get install libboost-all-dev`

For compilation instructions see: http://wiki.blender.org/index.php/Dev:Doc/Building_Blender/Linux

It is recomended to use scons for compilation